# Nat

plus_n_O: forall n : nat, n = n + 0
plus_Sn_m: forall n m : nat, S n + m = S (n + m)
Pumping.a_le_b__or__a_gt_b : forall a b : nat, a <= b \/ b < a.


# Lists

app_nil_r: forall (X : Type) (l : list X), l ++ [ ] = l
app_assoc : forall (A : Type) (l m n : list A), l ++ m ++ n = (l ++ m) ++ n
app_length: forall (X : Type) (l1 l2 : list X), length (l1 ++ l2) = length l1 + length l2
empty_list_and: forall (T : Type) (l1 l2 : list T), l1 ++ l2 = [ ] -> l1 = [ ] /\ l2 = [ ]
rev_app_distr: forall (X : Type) (l1 l2 : list X), rev (l1 ++ l2) = rev l2 ++ rev l1

# String
String.eqb s1 s2 : bool ;; also (s1 =? s2)%string : bool
string_dec : forall (s1 s2 : string), {s1 = s2} + {s1 <> s2}

# reflect

destruct (eqbP n m) as [H|H].
destruct (ascii_dec t t) eqn:K.
destruct (String.eqb_spec x1 x) as [J|K].
