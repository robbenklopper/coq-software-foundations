An airport modelled as colored petri net via CPNTools. Passengers are
registered and tracked: entering the terminal and boarding the plane. Airplanes
are registered to flights. Touch down and take off are tracked to lock lanes
for exactly one aircraft.

See `airport-fith-draft.cpn` for the final result. 
