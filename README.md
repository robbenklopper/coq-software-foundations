In case working on one of the `.v` files in `logical foundations` complains
that dependencies are missing, you need to run `make` in that directory. This
is typically the case when doing a fresh checkout or if the remote has a lot
of new code. As compiled files are not intended to be under version control,
the user is exposed to the 'inconvenience' of having to run `make` from time to
time.

See `logical foundations/Induction.v` for instructions on how `make` is meant
to be used here.
